USE [RPA]
GO

/****** Object:  Table [dbo].[NIP_SETTLEMENT_TEMP]    Script Date: 3/29/2021 9:36:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NIP_SETTLEMENT_TEMP](
	[CHANNEL] [varchar](max) NULL,
	[AMOUNT] [varchar](max) NULL,
	[NARRATION] [varchar](max) NULL,
	[SETTLEMENTTYPE] [varchar](max) NULL,
	[SESSION] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


