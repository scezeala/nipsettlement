USE [RPA]
GO

/****** Object:  Table [dbo].[NIBSS_POSTING]    Script Date: 3/29/2021 9:35:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NIBSS_POSTING](
	[DETBSJRNL] [varchar](3) NULL,
	[BRN] [varchar](3) NULL,
	[BATCHNO] [varchar](1) NULL,
	[SRCCODE] [varchar](9) NULL,
	[AMOUNT] [decimal](38, 2) NULL,
	[ACNO] [varchar](10) NULL,
	[DRCR] [varchar](1) NULL,
	[AC_BRN] [varchar](3) NULL,
	[TXNCD] [varchar](3) NULL,
	[VALDT] [varchar](8) NULL,
	[INSTNO] [varchar](1) NULL,
	[SessionID] [varchar](5) NULL,
	[ADDLTEXT] [nvarchar](235) NULL,
	[COST_CENTER] [varchar](4) NULL,
	[reconDate] [varchar](11) NULL
) ON [PRIMARY]
GO


